<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Server Match - Encontre profissionais de tecnologia e agende sua aula!</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/estilo.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="http://propeller.in/components/textfield/css/textfield.css">

</head>

<style>
    p {
        color: #555555;
        font-size: 18px;
    }

    h1 {
        font-size: 40px;
        padding-top: 25px;
        color: #e91e63;
    }

    h2 {
        font-size: 25px;
        padding-top: 5px;
        padding-bottom: 20px;
    }

    h3 {
        color: white;
        font-size: 36px;
    }

    #conteudo_prof {

        margin-top:30px;
        padding: 70px;
        border-radius: 20px;
        background-image: linear-gradient(112deg, #e91e63 , #170085);

    }
</style>

<body>
    <?php
    include 'conexao.php';
    
    session_start();
    
    if(isset($_SESSION['usuarioLogado'])){
      $usuario = $_SESSION['usuarioLogado'];
      $id = $_SESSION['idUsuarioLogado'];
      $nivel = $_SESSION['nivelUsuarioLogado'];
      $nome = $_SESSION['nomeUsuarioLogado'];
  }

  include 'cabecalho.php';

  ?>
  <main>
    <section class="container" style="margin-top: 20px">

        <header class="card-title text-center">
            <h1><strong>Como Funciona?</strong></h1>
            <h2 style="color:#555555"> Ajudamos você a encontrar os melhores professores de tecnologia do Brasil, de
            acordo com as suas necessidades.</h2>
        </header>

        <div class="col-12">


            <section class="col-12" id="title">
                <div>
                    <h4>Inscreva-se</h4>
                    <p>Para visualizar os anúncios de aulas, é essencial possuir um cadastro na plataforma.</p>
                </div>

            </section>


            <section class="col-12" id="title">
                <div>
                    <h4>Escolha um professor</h4>
                    <p>Encontre professores de acordo com as qualificações e as suas preferências.</p>
                </div>

            </section>


            <section class="col-12" id="title">
                <div>
                    <h4>Verifique os detalhes</h4>
                    <p>Verifique todos os detalhes do anúncio antes de tomar uma decisão e enviar uma
                        solicitação de
                    aula.</p>
                </div>
            </section>




            <section class="col-12" id="title">
                <div>
                    <h4>Enviar Solicitação</h4>
                    <p>Encontrou o professor perfeito? Basta clicar no ícone de match para solicitar aula.</p>
                    <p>O professor pode confirmar ou cancelar o match recebido.
                        O status de solicitação de aula será atualizado automaticamente quando o professor
                        confirmar
                    ou cancelar o match.</p>
                </div>
            </section>


            <section class="col-12" id="title">
                <div>
                    <h4>Solicitações enviadas</h4>
                    <p>No painel de solicitações enviadas, e possível gerenciar e acompanhar as solicitações
                    pendentes, confirmadas e canceladas.</p>
                    <p>Após a confirmação do professor, visualize as informações de contato e agende sua aula!
                    </p>
                </div>
            </section>

            <section class="col-12" id="conteudo_prof">
                <h3>Quero ser um Professor!</h3>
                <div>
                    <h4 class="pt-4" style="color:white">Crie seu anúncio gratuitamente</h4>
                    <p style="color:white">Não cobramos nenhuma taxa de inscrição para criar seu anúncio e compartilhar seus conhecimentos.</p>
                </div>
                <div>
                    <h4 class="pt-4" style="color:white">Escolha suas condições</h4>
                    <p style="color:white">Você tem controle total sobre a sua disponibilidade, preços, metodologia e trabalho. Você organiza seus cursos em total liberdade.</p>
                </div>
            </section>


        </div>

    </section>

    

</main>

<?php
include 'rodape.php';
?>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
</script>
<script src="http://propeller.in/components/global/js/global.js"></script>
<script type="text/javascript" src="http://propeller.in/components/textfield/js/textfield.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
<script src="https://kit.fontawesome.com/1d6f563437.js" crossorigin="anonymous"></script>

<script type="text/javascript">
    $("#telefone").mask("(99) 99999-9999");
</script>


</body>


</html>