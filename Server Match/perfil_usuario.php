<?php

include 'conexao.php';

session_start();

$usuario = $_SESSION['usuarioLogado'];
$id = $_SESSION['idUsuarioLogado'];
$nome = $_SESSION['nomeUsuarioLogado'];

if(!isset($_SESSION['usuarioLogado'])){
    header('Location:index.php');
}


?>


<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Meu Perfil</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/estilo.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="http://propeller.in/components/textfield/css/textfield.css">


    <style type="text/css">

    </style>

</head>

<body>


    <?php

        if (isset($_POST['pesquisar'])){

            $pesquisar = $_POST['pesquisar'];
            $_SESSION['pesquisar'] = $pesquisar;
            
            echo "<meta HTTP-EQUIV='refresh' CONTENT='0;URL=index.php'>";

        }
        $sql = "SELECT * FROM usuario WHERE id = $id";
        $buscar = mysqli_query($conexao, $sql);

        while ($array = mysqli_fetch_array($buscar)) {
        $_SESSION['nomeUsuarioLogado'] = $array['nome'];
        $_SESSION['fotoUsuarioLogado'] = $array['foto'];
        include 'cabecalho.php';

        
        ?>

    <main style="background:#f7f7f7;">

        <section class="container pb-4" style="background:#f7f7f7;">

            <div class="row">

                <section class="col">
                    <h4 style="color:#555555; margin-top: 10px; padding: 10px;">
                        Perfil</h4>
                </section>

                <section class="col-auto mr-auto pt-2">
                    <?php 

                        if(isset($_SESSION['msg'])){ ?>

                    <div class="alert-info alert alert-primary alert-dismissible fade show" role="alert">
                        <?php echo $_SESSION['msg']; ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <script>
                    setTimeout(function() {
                        $('.alert-info').remove();
                    }, 5000);
                    </script>

                    <?php unset($_SESSION['msg']); } ?>

                </section>
            </div>

            <div class="row">

                <section class="col-12 col-md-5 col-lg-3">

                    <section class="card" style="border-radius: 20px;">
                        <div class="card-body">
                            <h5 class="card-title p-2" id="title">Foto do Perfil</h5>
                            <form action="" method="post" style="margin-top: 5px" enctype="multipart/form-data">

                                <div class="form-row justify-content-center">

                                    <input type="number" class="form-control" name="id"
                                        value="<?php echo $array['id'] ?>" style="display: none">

                                </div>

                                <div class="form-row justify-content-center">
                                    <div class="form-group">

                                        <label for="file-upload" class="custom-file-upload">
                                            <img src="img/avatar.png" alt="foto perfil"
                                                style="width:130px; height:130px;">
                                        </label>
                                        <input type="file" id="file-upload" name="foto" required>


                                    </div>
                                </div>

                                <div class="form-row justify-content-center">
                                    <button type="submit" id="" class="btn-cadastrar">Atualizar</button>
                                </div>

                            </form>

                            <?php

                                    if(isset($_FILES['foto'])){

                                        $ext = strtolower(substr($_FILES['foto']['name'],-4)); //Pegando extensão do arquivo

                                        if($ext == '.jpg' || $ext== '.png'){

                                            $new_name = $usuario . $ext; //Definindo um novo nome para o arquivo
                                            $dir = './imagens/'; //Diretório para uploads
    
                                            move_uploaded_file($_FILES['foto']['tmp_name'], $dir.$new_name); //Fazer upload do arquivo
    
                                            $sql = "UPDATE usuario SET foto = '$new_name' WHERE id = $id";
    
                                            $inserir = mysqli_query($conexao, $sql);
    
                                            echo '<div class="text-center mt-2">
                                            <div class="spinner-border"style="color:#170085" role="status">
                                            <span class="sr-only">Loading...</span>
                                            </div>
                                            </div>';
    
                                            echo "<meta HTTP-EQUIV='refresh' CONTENT='2;URL=perfil_usuario.php'>";


                                            }else{

                                                $_SESSION['msg'] =  "Formato do arquivo é inválido! 😄";

                                                echo "<meta HTTP-EQUIV='refresh' CONTENT='0;URL=perfil_usuario.php'>";

                                    }}

                                    ?>



                        </div>
                    </section>

                    <section class="card" style="margin-top: 20px; border-radius: 20px">
                        <div class="card-body">
                            <h5 class="card-title p-2" id="title">Dados de Acesso</h5>
                            <form action="_atualizar_senha.php" method="post" style="margin-top: 20px">

                                <input type="number" class="form-control" name="id" value="<?php echo $array['id'] ?>"
                                    style="display: none">

                                <div class="form-row justify-content-center">

                                    <div class="col-10 form-group pmd-textfield">
                                        <label for="Large" class="control-label">E-mail</label>
                                        <input name="email" class="form-control text-center" type="text"
                                            value="<?php echo $array['email'] ?>" autocomplete="off" disabled>
                                    </div>


                                </div>

                                <div class="form-row justify-content-center">

                                    <div class="col-10 form-group pmd-textfield">
                                        <label for="Large" class="control-label">Senha</label>
                                        <input id="txtSenha" name="senha" minlength="6" maxlength="18"
                                            title="Sua senha deve possuir entre 6 (seis) e 18 (dezoitos) digitos."
                                            class="form-control text-center" type="password" autocomplete="off"
                                            required>
                                    </div>

                                </div>

                                <div class="form-row justify-content-center">

                                    <div class="col-10 form-group pmd-textfield">
                                        <label for="Large" class="control-label">Repetir senha</label>
                                        <input name="repetirsenha" minlength="6" maxlength="18"
                                            class="form-control text-center" type="password" autocomplete="off" required
                                            oninput="validaSenha(this)">
                                    </div>

                                </div>

                                <div class="form-row justify-content-center" style="margin-top: 25px">
                                    <button type="submit" id="" class="btn-cadastrar">Atualizar</button>
                                </div>

                            </form>
                        </div>
                    </section>

                </section>

                <section class="col-12 col-md-7 col-lg-5">
                    <div class="card" style="border-radius: 20px;">
                        <div class="card-body">
                            <h5 class="card-title p-2" id="title">Dados Pessoais</h5>
                            <form action="_atualizar_dados_pessoais.php" method="post" style="margin-top: 20px">

                                <div class="form-row justify-content-center">

                                    <div class="col-10 form-group pmd-textfield">
                                        <label for="Large" class="control-label">Nome</label>
                                        <input name="nome" maxlength="35" class="form-control" type="text"
                                            value="<?php echo $array['nome'] ?>" autocomplete="off" required>
                                    </div>

                                </div>

                                <input type="number" class="form-control" name="id" value="<?php echo $array['id'] ?>"
                                    style="display: none">

                                <div class="form-row justify-content-center">

                                    <div class="col-10 form-group pmd-textfield">
                                        <label for="Large" class="control-label">Data de nascimento</label>
                                        <input id="campoData" name="nascimento" class="form-control" type="data"
                                            value="<?php echo $array['data_nascimento'] ?>" autocomplete="off">
                                    </div>

                                </div>

                                <div class="form-row justify-content-center">

                                    <div class="col-10 form-group pmd-textfield">
                                        <label>Localidade</label>
                                        <select name="uf" class="form-control">
                                            <option><?php echo $array['uf'] ?></option>
                                            <option>Acre</option>
                                            <option>Alagoas</option>
                                            <option>Amapá</option>
                                            <option>Amazonas</option>
                                            <option>Bahia</option>
                                            <option>Ceará</option>
                                            <option>Distrito Federal</option>
                                            <option>Espirito Santo</option>
                                            <option>Goiás</option>
                                            <option>Maranhão</option>
                                            <option>Mato Grosso do Sul</option>
                                            <option>Mato Grosso</option>
                                            <option>Minas Gerais</option>
                                            <option>Pará</option>
                                            <option>Paraíba</option>
                                            <option>Paraná</option>
                                            <option>Pernambuco</option>
                                            <option>Piauí</option>
                                            <option>Rio de Janeiro</option>
                                            <option>Rio Grande do Norte</option>
                                            <option>Rio Grande do Sul</option>
                                            <option>Rondônia</option>
                                            <option>Roraima</option>
                                            <option>Santa Catarina</option>
                                            <option>São Paulo</option>
                                            <option>Sergipe</option>
                                            <option>Tocantins</option>
                                        </select>
                                    </div>

                                </div>

                                <div class="form-row justify-content-center">

                                    <div class="col-10 form-group pmd-textfield">
                                        <label for="Large" class="control-label">Profissão</label>
                                        <input id="campoProfissao" maxlength="40" name="profissao" class="form-control"
                                            type="text" value="<?php echo $array['profissao'] ?>" autocomplete="off">
                                    </div>

                                </div>

                                <div class="form-row justify-content-center">

                                    <div class="col-10 form-group pmd-textfield">
                                        <label>Formação</label>
                                        <select name="formacao" class="form-control">
                                            <option><?php echo $array['formacao'] ?></option>
                                            <option>Curso Livre</option>
                                            <option>Ensino Fundamental</option>
                                            <option>Ensino Médio</option>
                                            <option>Curso Técnico</option>
                                            <option>Graduação</option>
                                            <option>Especialização</option>
                                            <option>MBA</option>
                                            <option>Mestrado</option>
                                            <option>Doutorado</option>
                                            <option>Pós Doutorado</option>
                                        </select>

                                    </div>

                                </div>

                                <div class="form-row justify-content-center">

                                    <div class="col-10 form-group pmd-textfield">
                                        <label for="Large" class="control-label">Curso</label>
                                        <input id="campoCurso" maxlength="40" name="curso" class="form-control"
                                            type="text" value="<?php echo $array['curso'] ?>" autocomplete="off">
                                    </div>

                                </div>

                                <div class="form-row justify-content-center">

                                    <div class="col-10 form-group pmd-textfield">
                                        <label for="Large" class="control-label">Instituição</label>
                                        <input id="campoInstituicao" maxlength="40" name="instituicao"
                                            class="form-control" type="text" value="<?php echo $array['instituicao'] ?>"
                                            autocomplete="off">
                                    </div>

                                </div>




                                <div class="form-row justify-content-center" style="margin-top: 33px">
                                    <button type="submit" id="" class="btn-cadastrar">Atualizar</button>
                                </div>



                            </form>
                        </div>
                    </div>
                </section>

                <section class="col-12 col-lg-4">
                    <div class="card" style="border-radius: 20px;">
                        <div class="card-body">
                            <h5 class="card-title p-2" id="title">Contato</h5>
                            <form action="_atualizar_contato.php" method="post" style="margin-top: 20px">

                                <div class="form-row justify-content-center">

                                    <input type="number" class="form-control" name="id"
                                        value="<?php echo $array['id'] ?>" style="display: none">

                                    <div class="col-10 form-group pmd-textfield">
                                        <label for="Large" class="control-label">Telefone</label>
                                        <input id="" name="telefone" class="form-control campoTelefone" type="text"
                                            value="<?php echo $array['telefone'] ?>" autocomplete="off" required>
                                    </div>

                                    <div class="col-10 form-group pmd-textfield">
                                        <label for="Large" class="control-label">Whatsapp</label>
                                        <input id="" name="whatsapp" class="form-control campoTelefone" type="text"
                                            value="<?php echo $array['whatsapp'] ?>" autocomplete="off">
                                    </div>

                                    <div class="col-10 form-group pmd-textfield">
                                        <label for="Large" class="control-label">E-mail</label>
                                        <input name="email" class="form-control" type="text"
                                            value="<?php echo $array['email'] ?>" autocomplete="off" disabled>
                                    </div>

                                    <div class="col-10 form-group pmd-textfield">
                                        <label for="Large" class="control-label">Skype</label>
                                        <input name="skype" maxlength="50" class="form-control" type="text"
                                            value="<?php echo $array['skype'] ?>" autocomplete="off">
                                    </div>

                                    <div class="col-10 form-group pmd-textfield">
                                        <label for="Large" class="control-label">Hangouts</label>
                                        <input name="hangouts" maxlength="50" class="form-control" type="text"
                                            value="<?php echo $array['hangouts'] ?>" autocomplete="off">
                                    </div>


                                    <div class="col-10 form-group pmd-textfield">
                                        <label for="Large" class="control-label">Linkedin</label>
                                        <input name="linkedin" maxlength="100" class="form-control" type="text"
                                            value="<?php echo $array['linkedin'] ?>" autocomplete="off">
                                    </div>

                                </div>


                                <div class="form-row justify-content-center" style="margin-top: 120px;">
                                    <button type="submit" id="" class="btn-cadastrar">Atualizar</button>

                                </div>

                            </form>
                        </div>
                    </div>
                </section>

            </div>

            <?php } ?>

        </section>

    </main>

    <?php include 'rodape.php'; ?>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>

    <script src="http://propeller.in/components/global/js/global.js"></script>
    <script type="text/javascript" src="http://propeller.in/components/textfield/js/textfield.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
    <script src="https://kit.fontawesome.com/1d6f563437.js" crossorigin="anonymous"></script>

    <script type="text/javascript">
    $(".campoTelefone").mask("(99) 99999-9999");
    $("#campoData").mask("99/99/9999");
    </script>

    <script>
    function validaSenha(input) {
        if (input.value != document.getElementById('txtSenha').value) {
            input.setCustomValidity('Repita a senha corretamente');

        } else {

            input.setCustomValidity('');
        }
    }
    </script>


</body>

</html>