<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Server Match - Encontre profissionais de tecnologia e agende sua aula!</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/estilo.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="http://propeller.in/components/textfield/css/textfield.css">

</head>

<style>
    p {
        color: #170085;
        font-size: 18px;
    }

    h1 {
        font-size: 40px;
        padding-top: 25px;
        color: #e91e63;
    }

    h2 {
        font-size: 25px;
        padding-top: 5px;
        padding-bottom: 20px;
    }

    h3 {
        color: white;
        font-size: 25px;
    }

    #conteudo_prof {

        margin-top: 30px;
        padding: 70px;
        border-radius: 20px;
        background-image: linear-gradient(112deg, #e91e63, #170085);

    }
</style>

<body>
    <?php
    include 'conexao.php';
    
    session_start();
    
    if(isset($_SESSION['usuarioLogado'])){
      $usuario = $_SESSION['usuarioLogado'];
      $id = $_SESSION['idUsuarioLogado'];
      $nivel = $_SESSION['nivelUsuarioLogado'];
      $nome = $_SESSION['nomeUsuarioLogado'];
  }

  include 'cabecalho.php';

  ?>
  <main>
    <section class="container" style="margin-top: 20px">

        <header class="card-title text-center">
            <h1><strong>Quem Somos?</strong></h1>
            <h2>Todos têm algo a ensinar e compartilhar.</h2>
        </header>

        <div class="col-12">


            <section class="col-12 text-center">
                <img src="img/map.png" alt="">
            </section>


            <section class="col-12 text-center" id="conteudo_prof">
                <h3>O Server Match é uma plataforma de aulas particulares de tecnologia.</h3>
                <div>
                    <p style="color:white">Nosso objetivo é facilitar e desenvolver uma comunidade com diversos
                    professores e alunos interessados em ensinar e aprender.</p>
                </div>
                <div>
                    <p style="color:white">As gerações contemporâneas estão cada vez mais engajadas e acostumadas
                        com a tecnologia, o momento atual é propício para disponibilizarmos novas ferramentas
                        educacionais, proporcionando um relacionamento humano-humano para um ensino efetivo a
                    distância.</p>
                </div>
            </section>


        </div>

    </section>



</main>

<?php
include 'rodape.php';
?>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
</script>
<script src="http://propeller.in/components/global/js/global.js"></script>
<script type="text/javascript" src="http://propeller.in/components/textfield/js/textfield.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
<script src="https://kit.fontawesome.com/1d6f563437.js" crossorigin="anonymous"></script>

<script type="text/javascript">
    $("#telefone").mask("(99) 99999-9999");
</script>


</body>


</html>