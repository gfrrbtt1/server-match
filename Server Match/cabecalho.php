<header>
    <nav class="navbar navbar-expand-lg navbar-light" id="cabecalho" style="-webkit-box-shadow: 0 5px 6px -6px #fb3c61;
    -moz-box-shadow: 0 5px 6px -6px #777;
    box-shadow: 0 5px 6px -6px #777;">

    <div class="col-lg-3 text-center">
        <a href="index.php"><img src="img/1.png" width="260px" height="80px" alt=""></a>
        <button class="navbar-toggler col-2" type="button" data-toggle="collapse"
        data-target="#conteudoNavbarSuportado" aria-controls="conteudoNavbarSuportado" aria-expanded="false"
        aria-label="Alterna navegação">
        <span class="navbar-toggler-icon"></span>
    </button>
</div>

<div class="col-12 col-lg-4 pt-2 pb-2  text-center">

    <form method="post" action="">
        <div class="col-12 form-row justify-content-center">
            <div class="col-9 col-md-8 col-lg-9">
                <input type="search" id="borda-input" class="form-control form-control-md text-center"
                placeholder="Buscar aulas" name="pesquisar" autocomplete="off">
            </div>

            <div class="col-3 col-md-2 col-lg-3">
                <button type="submit" id="buscar" class=" btn btn-block btn-md btn-outline-light"><i
                    class="fas fa-search"></i>&nbsp;</button>
                </div>
            </div>
        </form>

    </div>

    <div class="collapse navbar-collapse col-lg-5 text-center" id="conteudoNavbarSuportado">
        <div class="col-12 form-row justify-content-center pt-1 pb-2">

            <?php
            if (!isset($_SESSION['usuarioLogado'])){
                ?>

                <div class="col-12">
                    <a href="" id="btn-cadastrar" class="btn col-5" data-toggle="modal" data-target="#myModal-cadastro">
                    Inscreva-se</a>
                    <a href="" id="login" class="btn col-5" data-toggle="modal" data-target="#myModal-login">
                    Login</a>
                </div>

            </div>

            <?php
            include 'modal_login_cadastro.php';
            ?>

        <?php }else if(isset($_SESSION['usuarioLogado'])){  ?>


            <div class="col-12 col-lg-6 form-row">

                <div class="col-12 pt-1">
                    <a href="cadastrar_anuncio.php" id="btn-cadastrar" class="btn col-12 col-md-10 col-lg-12"
                    style="margin-top:28px;"><i class="fas fa-plus"></i>&nbsp;Criar um anúncio</a>
                </div>

            </div>

            <div class="col-12 col-lg-6 form-row">

                <div class="col-6">
                    <h6 class="mt-3 text-right" style="color:#fb3c61">Olá,</h6>

                    <?php
                    $var = $_SESSION['nomeUsuarioLogado'];

                    $nome_usuario = explode(" ", $var);
                    ?>

                    <h5 class="text-right" style="color:#170085"><?php echo $nome_usuario[0] ?></h5>

                </div>

                <div class="navbar-nav col-12 col-lg-6">
                    <div class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                        <?php if($_SESSION['fotoUsuarioLogado'] != null){ ?>

                            <img src="./imagens/<?php echo $_SESSION['fotoUsuarioLogado'] ?>"
                            class="rounded-circle z-depth-0" alt="avatar image"
                            style="cursor:pointer; width:80px; height:80px; border: 2px solid #f7f7f7 !important;">

                        <?php } else { ?>

                            <img src="img/foto.png" class="rounded-circle z-depth-0" alt="avatar image"
                            style="width:80px; height:80px; !important;">


                        <?php }?>
                    </a>

                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="perfil_usuario.php">Meu Perfil</a>
                        <a class="dropdown-item" href="meus_anuncios.php">Meus Anúncios</a>
                        <div class="dropdown-divider"></div>
                        <li class="dropdown-header">Solicitações</li>
                        <a class="dropdown-item" href="solicitacoes_enviadas.php">Enviadas</a>
                        <a class="dropdown-item" href="solicitacoes_recebidas.php">Recebidas</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="logout.php">Sair</a>
                    </div>
                </div>
            </div>
        </div>

    <?php } ?>
</div>

</nav>

</header>