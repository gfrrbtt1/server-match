<?php

include 'conexao.php';

$itens_por_pagina = 6;
$pagina = (isset($_GET['pagina']))? $_GET['pagina'] : 1;

session_start();

$usuario = $_SESSION['usuarioLogado'];
$id = $_SESSION['idUsuarioLogado'];
$nome = $_SESSION['nomeUsuarioLogado'];

if(!isset($_SESSION['usuarioLogado'])){
    header('Location:index.php');
}

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Solicitações recebidas</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/estilo.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="http://propeller.in/components/textfield/css/textfield.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
</script>


<style type="text/css">
    .filtro {
        border-radius: 40px;
        border: 1px solid #f7f7f7;
        background-color: #f7f7f7;
        color: #555555;
        font-weight: bolder;
        text-align: center;
    }

    .filtro:hover,
    .filtro:active {
        border-radius: 40px;
        border: 1px solid #170085 !important;
        background-color: #170085 !important;
        color: #ffffff !important;
        font-weight: bolder;
        text-align: center;
    }

    .cancelar:hover,
    .cancelar:active {
        border-radius: 40px;
        border: 1px solid #170085 !important;
        background-color: #170085 !important;
        color: #ffffff !important;
        font-weight: bolder;
        text-align: center;
    }

    .cancelar {
        border-radius: 40px;
        border: 1px solid #f7f7f7 !important;
        background-color: #f7f7f7 !important;
        color: #170085 !important;
        font-weight: bolder;
        text-align: center;
    }

    .confirmar:hover,
    .confirmar:active {
        border-radius: 40px;
        border: 1px solid #e91e63 !important;
        background-color: #e91e63 !important;
        color: #ffffff !important;
        font-weight: bolder;
        text-align: center;
    }

    .confirmar {
        border-radius: 40px;
        border: 1px solid #f7f7f7 !important;
        background-color: #f7f7f7 !important;
        color: #e91e63 !important;
        font-weight: bolder;
        text-align: center;
    }

    .acao:hover,
    .acao:active {
        border-radius: 40px;
        border: 1px solid #555555 !important;
        background-color: #555555 !important;
        color: #ffffff !important;
        font-weight: bolder;
        text-align: center;
    }

    .acao {
        border-radius: 40px;
        border: 1px solid #f7f7f7 !important;
        background-color: #f7f7f7 !important;
        color: #555555 !important;
        font-weight: bolder;
        text-align: center;
    }
</style>

</head>

<body>

    <?php 
    
    include 'cabecalho.php'; 

    if (isset($_POST['pesquisar'])){

        $pesquisar = $_POST['pesquisar'];
        $_SESSION['pesquisar'] = $pesquisar;
        
        echo "<meta HTTP-EQUIV='refresh' CONTENT='0;URL=index.php'>";

    }


    $sql = "SELECT * FROM solicitacoes WHERE id_professor = $id AND status_solicitacao = 'confirmado'";
    $buscar = mysqli_query($conexao, $sql);
    $total_confirmadas = mysqli_num_rows($buscar);

    $sql = "SELECT * FROM solicitacoes WHERE id_professor = $id AND status_solicitacao = 'pendente'";
    $buscar = mysqli_query($conexao, $sql);
    $total_pendentes = mysqli_num_rows($buscar);

    $sql = "SELECT * FROM solicitacoes WHERE id_professor = $id AND status_solicitacao = 'cancelado'";
    $buscar = mysqli_query($conexao, $sql);
    $total_canceladas = mysqli_num_rows($buscar);

    
    ?>

    <main style="background:#f7f7f7;">

        <section class="container pb-4" style="background:#f7f7f7; height: 900px">

            <section class="row">

                <div class="col">
                    <h4 style="color:#555555; margin-top: 10px; padding: 10px;">
                    Solicitações Recebidas</h4>
                </div>

                <div class="col-auto mr-auto pt-2">
                    <?php 

                    if(isset($_SESSION['msg'])){ ?>

                        <div class="alert-info alert alert-primary alert-dismissible fade show" role="alert">
                            <?php echo $_SESSION['msg']; ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <script>
                            setTimeout(function() {
                                $('.alert-info').remove();
                            }, 5000);
                        </script>

                        <?php unset($_SESSION['msg']); } ?>
                    </div>

                </section>

                <section class="row mb-4">

                    <div class="col-12">

                        <section class="card" style="border-radius: 20px;">
                            <div class="card-body">
                                <section class="form-row mb-3">

                                    <div class="col-12 col-lg-2 pt-2">
                                        <h5 class="card-title p-2" id="title">Pesquisar por</h5>
                                    </div>


                                    <div class="col-lg-6 pt-2">

                                        <form class="form-row" name="filtro" method="post" action="">
                                            <div class="col-4 text-center">


                                                <button class="col-12 filtro btn" name="pendentes" id="pendente"
                                                type="submit" id="pendente">Pendentes
                                                <span class="badge"><?php echo $total_pendentes ?></span></button>


                                            </div>


                                            <div class="col-4 text-center">

                                                <button class="col-12 filtro btn" name="confirmadas" id="confirmadas"
                                                type="submit">Confirmadas
                                                <span class="badge"><?php echo $total_confirmadas ?></span></button>

                                            </div>


                                            <div class="col-4 text-center">


                                                <button class="col-12 filtro btn" name="canceladas" id="canceladas"
                                                type="submit">Canceladas
                                                <span class="badge"><?php echo $total_canceladas ?></span></button>


                                            </div>
                                        </form>
                                    </div>

                                    <div class="col-12 col-lg-4 pt-2">
                                        <form class="form-row justify-content-center" method="post" action="">
                                            <div class="col-10 col-lg-7">
                                                <input type="text" id="borda-input"
                                                class="form-control form-control-md text-center"
                                                placeholder="titulo do anuncio" name="pesquisar_anuncio" required>
                                            </div>
                                            <div class="col-2">
                                                <button type="submit"
                                                class="filtro btn btn-block btn-md btn-outline-light"><i
                                                class="fas fa-search" style=""></i>&nbsp;</button>
                                            </div>
                                        </form>
                                    </div>

                                </section>
                                
                                <table class="table text-center">
                                    <thead>
                                        <tr style="color:#170085">
                                            <th scope="col"></th>
                                            <th scope="col">Solicitante</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Aula</th>
                                            <th scope="col">Categoria</th>
                                            <th scope="col">Tarifa</th>
                                            <th scope="col">Acao</th>
                                        </tr>
                                    </thead>

                                    <?php 

                                    if (isset($_POST['pesquisar_anuncio'])){

                                        $pesquisar = $_POST['pesquisar_anuncio'];
                                        $sql = "SELECT * FROM solicitacoes, anuncio, usuario WHERE id_professor = $id AND anuncio.id = solicitacoes.id_anuncio AND usuario.id = solicitacoes.id_aluno AND solicitacoes.titulo_anuncio LIKE '%$pesquisar%' AND anuncio.status = 'ativo'";
                                        $result = mysqli_query($conexao, $sql);
                                        $total = mysqli_num_rows($result);

                                        $num_pagina = ceil($total/$itens_por_pagina);

                                        $inicio = ($itens_por_pagina*$pagina)-$itens_por_pagina;
                                        
                                        $sql = "SELECT * FROM solicitacoes, anuncio, usuario WHERE id_professor = $id AND anuncio.id = solicitacoes.id_anuncio AND usuario.id = solicitacoes.id_aluno AND solicitacoes.titulo_anuncio LIKE '%$pesquisar%' AND anuncio.status = 'ativo' limit $inicio, $itens_por_pagina";
                                        $result = mysqli_query($conexao, $sql);
                                        
                                        if($total == 0){

                                           echo '<div class="alert alert-danger mt-3" role="alert" align="center">
                                           Opps! Nao encontramos nenhuma solicitacao de anuncio com esse titulo! 
                                           </div>';

                                       }

                                   } else if (isset($_POST['confirmadas'])){

                                    $sql = "SELECT * FROM solicitacoes, anuncio, usuario WHERE id_professor = $id AND anuncio.id = solicitacoes.id_anuncio AND usuario.id = solicitacoes.id_aluno and solicitacoes.status_solicitacao = 'confirmado' AND anuncio.status = 'ativo'";
                                    $result = mysqli_query($conexao, $sql);
                                    $total = mysqli_num_rows($result);
                                    $color = "confirmadas";

                                    $num_pagina = ceil($total/$itens_por_pagina);

                                    $inicio = ($itens_por_pagina*$pagina)-$itens_por_pagina;

                                    $sql = "SELECT * FROM solicitacoes, anuncio, usuario WHERE id_professor = $id AND anuncio.id = solicitacoes.id_anuncio AND usuario.id = solicitacoes.id_aluno and solicitacoes.status_solicitacao = 'confirmado' AND anuncio.status = 'ativo' limit $inicio, $itens_por_pagina";
                                    $result = mysqli_query($conexao, $sql);
                                    
                                    
                                } else if (isset($_POST['canceladas'])){
                                    
                                    $sql = "SELECT * FROM solicitacoes, anuncio, usuario WHERE id_professor = $id AND anuncio.id = solicitacoes.id_anuncio AND usuario.id = solicitacoes.id_aluno and solicitacoes.status_solicitacao = 'cancelado' AND anuncio.status = 'ativo'";
                                    $result = mysqli_query($conexao, $sql);
                                    $total = mysqli_num_rows($result);
                                    $color = "canceladas";

                                    $num_pagina = ceil($total/$itens_por_pagina);

                                    $inicio = ($itens_por_pagina*$pagina)-$itens_por_pagina;

                                    $sql = "SELECT * FROM solicitacoes, anuncio, usuario WHERE id_professor = $id AND anuncio.id = solicitacoes.id_anuncio AND usuario.id = solicitacoes.id_aluno and solicitacoes.status_solicitacao = 'cancelado' AND anuncio.status = 'ativo' limit $inicio, $itens_por_pagina";
                                    $result = mysqli_query($conexao, $sql);
                                    
                                }else{
                                    
                                    $sql = "SELECT * FROM solicitacoes, anuncio, usuario WHERE id_professor = $id AND anuncio.id = solicitacoes.id_anuncio AND usuario.id = solicitacoes.id_aluno and solicitacoes.status_solicitacao = 'pendente' AND anuncio.status = 'ativo'";
                                    $result = mysqli_query($conexao, $sql);
                                    $total = mysqli_num_rows($result);
                                    $color = "pendente";

                                    $num_pagina = ceil($total/$itens_por_pagina);

                                    $inicio = ($itens_por_pagina*$pagina)-$itens_por_pagina;

                                    $sql = "SELECT * FROM solicitacoes, anuncio, usuario WHERE id_professor = $id AND anuncio.id = solicitacoes.id_anuncio AND usuario.id = solicitacoes.id_aluno and solicitacoes.status_solicitacao = 'pendente' AND anuncio.status = 'ativo' limit $inicio, $itens_por_pagina";
                                    $result = mysqli_query($conexao, $sql);
                                    
                                }
                                ?>

                                <script>
                                    $("#<?php echo $color?>").css("background", "#170085", "color", "white");
                                    $("#<?php echo $color?>").css("color", "white");
                                </script>

                                <?php
                                while ($array = mysqli_fetch_array($result)) {
                                    $status = $array['status_solicitacao'];
                                    $foto = $array['foto'];

                                    ?>

                                    <tr>

                                        <td> <?php
                                        
                                        if($foto == null){ ?>

                                            <img src="img/foto.png" class="rounded-circle z-depth-0" alt="avatar image"
                                            style="cursor:pointer; width:50px; height:50px; border: 2px solid #f7f7f7 !important;">

                                        <?php }else { ?>

                                            <img src="./imagens/<?php echo $array['foto'] ?>"
                                            class="rounded-circle z-depth-0" alt="avatar image"
                                            style="cursor:pointer; width:50px; height:50px; border: 2px solid #f7f7f7 !important;">

                                        <?php } ?>
                                    </td>
                                    <td><?php echo $array['nome_solicitante'] ?></td>
                                    <td><?php echo $array['status_solicitacao'] ?></td>
                                    <td><?php echo $array['titulo_anuncio'] ?></td>
                                    <td><?php echo $array['categoria_anuncio'] ?></td>
                                    <td><?php echo $array['tarifa_anuncio'] ?></td>

                                    <td>

                                        <?php
                                        if($status == "confirmado"){ ?>

                                            <a class="confirmar btn btn-sm"
                                            href="_finalizar_solicitacao.php?id=<?php echo $array['id_anuncio']; ?>&solicitacao=<?php echo $array['id_solicitacao']; ?>"
                                            role="button"><i class="fas fa-check-square"></i>&nbsp;Finalizar</a>

                                            <a class="cancelar btn btn-sm"
                                            href="_cancelar_solicitacao.php?id=<?php echo $array['id_anuncio']; ?>&solicitacao=<?php echo $array['id_solicitacao']; ?>"
                                            role="button"><i class="fas fa-times-circle"></i>&nbsp;Cancelar</a>

                                            <a class="acao btn btn-sm"
                                            href="detalhe_anuncio.php?id=<?php echo $array['id_anuncio']; ?>&solicitacao=<?php echo $array['id_solicitacao']; ?>"
                                            role="button"><i class="fas fa-search-plus"></i>&nbsp;Visualizar</a>


                                        <?php } else if($status == "pendente") { ?>


                                            <a class="confirmar btn btn-sm"
                                            href="_confirmar_solicitacao.php?id=<?php echo $array['id_anuncio']; ?>&solicitacao=<?php echo $array['id_solicitacao']; ?>"
                                            role="button"><i class="fas fa-check-circle"></i>&nbsp;Confirmar</a>

                                            <a class="cancelar btn btn-sm"
                                            href="_cancelar_solicitacao.php?id=<?php echo $array['id_anuncio']; ?>&solicitacao=<?php echo $array['id_solicitacao']; ?>"
                                            role="button"><i class="fas fa-times-circle"></i>&nbsp;Cancelar</a>

                                            <a class="acao btn btn-sm"
                                            href="detalhe_anuncio.php?id=<?php echo $array['id_anuncio']; ?>&solicitacao=<?php echo $array['id_solicitacao']; ?>"
                                            role="button"><i class="fas fa-search-plus"></i>&nbsp;Visualizar</a>


                                        <?php } else { ?>

                                            <a class="acao btn btn-sm"
                                            href="_finalizar_solicitacao.php?id=<?php echo $array['id_anuncio']; ?>&solicitacao=<?php echo $array['id_solicitacao']; ?>"
                                            role="button"><i class="fas fa-eraser"></i>&nbsp;Excluir</a>

                                        <?php } ?>

                                    </td>


                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </section>
            </div>
        </section>

        <?php
                        //Verificar a pagina anterior e posterior
        $pagina_anterior = $pagina - 1;
        $pagina_posterior = $pagina + 1;
        ?>
        <nav aria-label="Navegação de página">
            <ul class="pagination justify-content-center pt-4">
                <li class="page-item">
                    <?php
                    if($pagina_anterior != 0){ ?>
                        <a class="page-link" href="solicitacoes_recebidas.php?pagina=<?php echo $pagina_anterior; ?>"
                            aria-label="Anterior">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    <?php }else{ ?>
                        <li class="page-item disabled">

                        </li>

                    <?php }  ?>
                </li>
                <?php 
                            //Apresentar a paginacao
                for($i = 1; $i < $num_pagina + 1; $i++){ 
                    $estilo ="";
                    if($pagina == $i){
                        $estilo = "class=\"page-item active\"";
                    } ?>

                    <li <?php echo $estilo; ?>><a class="page-link"
                        href="solicitacoes_recebidas.php?pagina=<?php echo $i; ?>"><?php echo $i; ?></a></li>
                    <?php } ?>
                    <li class="page-item">
                        <?php
                        if($pagina_posterior <= $num_pagina){ ?>
                            <a class="page-link" href="solicitacoes_recebidas.php?pagina=<?php echo $pagina_posterior; ?>"
                                aria-label="Próximo">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        <?php }else{ ?>
                            <li class="page-item disabled">

                            </li>
                        <?php }  ?>
                    </li>
                </ul>
            </nav>

        </section>

    </main>

    <?php
    include 'rodape.php';
    ?>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
    integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
</script>

<script src="http://propeller.in/components/global/js/global.js"></script>
<script type="text/javascript" src="http://propeller.in/components/textfield/js/textfield.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
<script src="https://kit.fontawesome.com/1d6f563437.js" crossorigin="anonymous"></script>


</body>

</html>