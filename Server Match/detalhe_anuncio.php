<?php

include 'conexao.php';

session_start();

$usuario = $_SESSION['usuarioLogado'];
$id = $_SESSION['idUsuarioLogado'];
$nome = $_SESSION['nomeUsuarioLogado'];

$id_anuncio = $_GET['id'];
$id_solicitacao = $_GET['solicitacao'];

?>


<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Detalhes da aula</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/estilo.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="http://propeller.in/components/textfield/css/textfield.css">


    <style type="text/css">
        .tooltip>.tooltip-inner {
            background-color: #FFFFFF;
            color: #170085;
            border: 1px solid black;
            padding: 15px;
            font-size: 12px;
        }


        .btn-badge {
            background-color: #FFFFFF;
            color: #170085;
            border: 1px solid #170085 !important;
        }

        .borda-pink {
            border-radius: 20px;
            border: 1px solid #fb3c61 !important;
        }

        .color-pink {
            color: #fb3c61;
        }

        textearea {
            resize: none;
            overflow-y: auto;
        }

        #contato {
            display: none;
        }

        #contato_professor {
            border-radius: 20px;
            border: 1px solid #fb3c61 !important;
            cursor: pointer;
        }

        .contato{
            color: #170085;
        }

        .contato a:hover{
            color: #e91e63;
            text-decoration:none; 

        }

        .contato a{
            color: #170085;
        }

    </style>

</head>

<body>

    <?php

    if (isset($_POST['pesquisar'])){

        $pesquisar = $_POST['pesquisar'];
        $_SESSION['pesquisar'] = $pesquisar;
        
        echo "<meta HTTP-EQUIV='refresh' CONTENT='0;URL=index.php'>";

    }


    $sql = "SELECT * from anuncio, usuario where anuncio.id = $id_anuncio and usuario.id = anuncio.id_usuario";
    $buscar = mysqli_query($conexao, $sql);
    

    $sql = "SELECT * FROM solicitacoes WHERE id_aluno = $id and id_anuncio = $id_anuncio ORDER BY id_solicitacao DESC LIMIT 1";
    $verificar_match = mysqli_query($conexao, $sql);       
    while ($array = mysqli_fetch_array($verificar_match)) {
      $id_solicitacao = $array['id_solicitacao'];

  }

  $total = mysqli_num_rows($buscar);

  if($total > 0){

      while ($array = mysqli_fetch_array($buscar)) {
        $id_usuario_anuncio = $array['id_usuario'];
        $foto = $array['foto'];
        include 'cabecalho.php';
        ?>

        <main>

            <section class="container pb-4">

                <div class="row justify-content-center">

                    <div class="col-10 col-md-7 col-lg-4 pt-4">

                        <div class="card" style="background: #f7f7f7; border-radius: 20px;">

                            <?php
                            if($foto == null){ ?>
                                <img class="card-img-top" src="img/foto.png" alt="Professor" style="width:100%; height:350px; border-radius: 20px;">

                            <?php }else { ?>

                                <img class="card-img-top" src="./imagens/<?php echo $array['foto'] ?>"
                                alt="Professor" style="width:100%; height:100%; border-radius: 20px;">

                            <?php } ?>

                        </div>


                        <div id="contato" class="col card mt-3" style="color:#555555; border-radius: 20px;">

                            <div class="card-body contato">

                                <h4 id="title" class="title p-2 mt-1">Contato</h4>
                                <p class="title p-1"><i class="fas fa-mobile-alt"></i>
                                    <?php echo $array['telefone'] ?></p>
                                    <p class="title p-1"><i class="fab fa-whatsapp"></i>
                                        <?php echo $array['whatsapp'] ?></p>
                                        <p class="title p-1"><i class="far fa-envelope"></i>
                                            <a href="mailto:<?php echo $array['email'] ?>" target="_blank"><?php echo $array['email'] ?></a></p>
                                            <p class="title p-1"><i class="fab fa-skype"></i>
                                                <?php echo $array['skype'] ?></p>
                                                <p class="title p-1"><i class="fab fa-google-plus-g"></i>
                                                    <a href="mailto:<?php echo $array['hangouts'] ?>" target="_blank"><?php echo $array['hangouts'] ?></a></p>
                                                    <p class="title p-1"><i class="fab fa-linkedin-in"></i>
                                                        <a href="<?php echo $array['linkedin'] ?>" target="_blank">visualizar perfil</a></p>

                                                    </div>


                                                </div>

                                            </div>

                                            <div class="col-12 col-md-8 pt-4">

                                                <form action="_inserir_solicitacao.php" method="post">

                                                    <input type="number" class="form-control" name="id_aluno" value="<?php echo $id ?>"
                                                    style="display: none">


                                                    <input type="number" class="form-control" name="id_professor"
                                                    value="<?php echo $array['id_usuario'] ?>" style="display: none">

                                                    <input type="number" class="form-control" name="id_anuncio" value="<?php echo $id_anuncio ?>"
                                                    style="display: none">

                                                    <input type="text" class="form-control" name="nome_solicitante" value="<?php echo $nome ?>"
                                                    style="display: none">

                                                    <input type="text" class="form-control" name="titulo" value="<?php echo $array['titulo'] ?>"
                                                    style="display: none">

                                                    <input type="text" class="form-control" name="tarifa" value="<?php echo $array['tarifa'] ?>"
                                                    style=" display: none">

                                                    <input type="text" class="form-control" name="categoria"
                                                    value="<?php echo $array['categoria'] ?>" style="display: none">


                                                    <div class="card text-center" style="background:; border-radius: 20px;">

                                                        <div class="card-body">
                                                            <div class="col-12 form-row">
                                                                <div class="col-12 col-md-6 text-left" id="title" style="">
                                                                    <h4 class="title pl-2 pt-2">
                                                                        <?php echo $array['nome'] ?></h4>
                                                                        <h5 class="title pl-2" style="color:#fb3c61;">
                                                                            <?php echo $array['profissao'] ?></h5>
                                                                            <h6 class="title pl-2" style="color:#555555;">
                                                                                <?php echo $array['uf'] ?></h6>


                                                                            </div>

                                                                            <div class="col-12 col-md-6 pl-3 text-left"
                                                                            style="background:#f7f7f7; color:#555555; border-radius: 25px;">
                                                                            <h6 class="title  pt-3">
                                                                                <?php echo $array['formacao'] ?></h6>
                                                                                <h6 class="title">
                                                                                    <?php echo $array['curso'] ?></h6>
                                                                                    <h6 class="title">
                                                                                        <?php echo $array['instituicao'] ?></h6>

                                                                                    </div>


                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="card text-center mt-3" style="background:; border-radius: 20px;">

                                                                            <div class="card-body">
                                                                                <div class="col-12 form-row">

                                                                                    <div class="col-12 col-md-5 text-left" id="title" style="">
                                                                                        <h4 class="title pl-2 pt-2">
                                                                                            <?php echo $array['titulo'] ?></h4>
                                                                                            <h5 class="title pl-2" style="color:#fb3c61;">
                                                                                                <?php echo $array['categoria'] ?></h5>


                                                                                            </div>

                                                                                            <div class="col-12 col-md-3" style="color:#555555; border-radius: 25px;">
                                                                                                <h4 class="title pt-4">
                                                                                                    <?php echo $array['tarifa'] ?>/h</h4>

                                                                                                </div>

                                                                                                <?php

                                                                                                if( $id != $id_usuario_anuncio){ ?>

                                                                                                    <div class="col-12 col-md-4">

                                                                                                        <?php

                                                                                                        $sql = "SELECT * FROM solicitacoes WHERE id_aluno = $id and id_anuncio = $id_anuncio and id_solicitacao = $id_solicitacao";
                                                                                                        $result = mysqli_query($conexao, $sql);
                                                                                                        $total = mysqli_num_rows($result);

                                                                                                        if($total > 0){

                                                                                                            while ($lista = mysqli_fetch_array($result)) {
                                                                                                                $status = $lista['status_solicitacao'];
                                                                                                                
                                                                                                                if($status == "pendente"){?>
                                                                                                                    <h6 class="title" style="color:#e91e63;">Status</h6>
                                                                                                                    <h6 class="title" style="color:#170085;">Aguardando confirmação de Match!</h6>

                                                                                                                <?php } else if($status == "confirmado"){ ?>
                                                                                                                    <h6 class="title" style="color:#e91e63;">Status</h6>
                                                                                                                    <h6 class="title" style="color:#170085;">Match confirmado!</h6>
                                                                                                                    <a class="btn p-2" id="viewContact" onclick="Mudarestado('contato')">Entrar em
                                                                                                                    contato</a>

                                                                                                                <?php } else { ?>

                                                                                                                    <button type="submit" class="btn" name="match" style="color:#170085; font-weight: bold;"><img src="img/match.png"
                                                                                                                        class="" name="match" alt="match"
                                                                                                                        style="width:70px; height:70px;">Enviar Match</button>

                                                                                                                    <?php }}}else{ ?>

                                                                                                                        <button type="submit" class="btn" name="match" style="color:#170085; font-weight: bold;"><img src="img/match.png"
                                                                                                                            class="" name="match" alt="match"
                                                                                                                            style="width:70px; height:70px;">Enviar Match</button>

                                                                                                                        <?php } ?>

                                                                                                                    </div>

                                                                                                                <?php } ?>
                                                                                                                
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </form>


                                                                                                <div class="card text-center mt-3" style="border-radius: 20px;">

                                                                                                    <div class="card-body">

                                                                                                        <div class="col-12">

                                                                                                            <h5 class="col-12 card-title p-2 mt-3 text-left" id="title">Modalidade
                                                                                                            </h5>

                                                                                                            <div class="row justify-content-center">
                                                                                                                <div class="modalidade-info p-2 col-5 mr-1">
                                                                                                                    <?php echo $array['modalidade'] ?>
                                                                                                                </div>

                                                                                                                <div class="modalidade-info p-2 col-5">

                                                                                                                    <?php echo $array['nivel'] ?></h6>
                                                                                                                </div>
                                                                                                            </div>


                                                                                                            <h5 class="col-12 card-title p-2 mt-3 text-left" id="title">Apresentação
                                                                                                            </h5>

                                                                                                            <div class="col-12 text-justify">
                                                                                                                <p><?php echo $array['apresentacao'] ?></p>
                                                                                                            </div>

                                                                                                            <h5 class="col-12 card-title p-2 mt-3 text-left" id="title">Metodologia
                                                                                                            </h5>

                                                                                                            <div class="col-12 text-justify">
                                                                                                                <p><?php echo $array['metodologia'] ?></p>
                                                                                                            </div>


                                                                                                            <h5 class="col-12 card-title p-2 mt-3 text-left" id="title">Tarifas</h5>


                                                                                                            <div class="col-12 text-justify">

                                                                                                                <p><?php echo $array['tarifa_detalhe'] ?></p>

                                                                                                            </div>

                                                                                                        </div>


                                                                                                    </div>

                                                                                                </div>

                                                                                            </div>
                                                                                        </div>

                                                                                    </section>


                                                                                <?php }}else{
                                                                                   header('Location: erro.php');
                                                                               } ?>



                                                                           </main>

                                                                           <?php
                                                                           include 'rodape.php';
                                                                           ?>


                                                                           <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                                                                           integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
                                                                       </script>
                                                                       <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
                                                                       integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
                                                                   </script>
                                                                   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
                                                                   integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
                                                               </script>

                                                               <script src="http://propeller.in/components/global/js/global.js"></script>
                                                               <script type="text/javascript" src="http://propeller.in/components/textfield/js/textfield.js"></script>
                                                               <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
                                                               <script src="https://kit.fontawesome.com/1d6f563437.js" crossorigin="anonymous"></script>

                                                               <script type="text/javascript">
                                                                $(document).ready(function() {
                                                                    $('[data-toggle="popover"]').popover();
                                                                });
                                                            </script>

                                                            <script>
                                                                function Mudarestado(el) {
                                                                    document.getElementById(el).style.display = 'block';
                                                                }
                                                            </script>

                                                            <!-- Chama o arquivo Bootstrap JavaScript -->
                                                        </body>

                                                        </html>