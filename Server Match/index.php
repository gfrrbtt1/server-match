<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Server Match - Encontre profissionais de tecnologia e agende sua aula!</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/estilo.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="http://propeller.in/components/textfield/css/textfield.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>

</head>

<body>
    <?php
    include 'conexao.php';
    
    $itens_por_pagina = 6;
    $pagina = (isset($_GET['pagina']))? $_GET['pagina'] : 1;
    $cadastroInvalido = false;
    $loginInvalido = false;

    session_start();
    
    if(isset($_SESSION['usuarioLogado'])){
      $usuario = $_SESSION['usuarioLogado'];
      $id = $_SESSION['idUsuarioLogado'];
      $nivel = $_SESSION['nivelUsuarioLogado'];
      $nome = $_SESSION['nomeUsuarioLogado'];
  }

  include 'cabecalho.php';

  ?>
    <main>
        <section class="container" style="margin-top: 20px">

            <header class="card-title p-2" id="title" style="margin-left: 21px">
                <h4><strong>Match no professor perfeito?</strong></h4>
                <h5 style="color:#555555"> Encontre profissionais de tecnologia e agende sua aula!</h5>
            </header>

            <div class="row">

                <?php

            if (isset($_POST['pesquisar'])){

                $pesquisar = $_POST['pesquisar'];
                $sql = "SELECT anuncio.id, anuncio.titulo, usuario.nome, usuario.foto, anuncio.tarifa from anuncio, usuario where anuncio.status = 'ativo' and  usuario.id = anuncio.id_usuario and anuncio.titulo LIKE '%$pesquisar%'";
                $busca = mysqli_query($conexao, $sql);
                $total = mysqli_num_rows($busca);

                $num_pagina = ceil($total/$itens_por_pagina);

                $inicio = ($itens_por_pagina*$pagina)-$itens_por_pagina;

                $sql = "SELECT anuncio.id, anuncio.titulo, usuario.nome, usuario.foto, anuncio.tarifa from anuncio, usuario where anuncio.status = 'ativo' and  usuario.id = anuncio.id_usuario and anuncio.titulo LIKE '%$pesquisar%' limit $inicio, $itens_por_pagina";
                $busca = mysqli_query($conexao, $sql);

                if($total == 0){

                    echo "<meta HTTP-EQUIV='refresh' CONTENT='0;URL=erro_busca.php'>";

                }

            }else if(isset($_SESSION['pesquisar'])){

                $retorno_pesquisa = $_SESSION['pesquisar'];
                $sql = "SELECT anuncio.id, anuncio.titulo, usuario.nome, usuario.foto, anuncio.tarifa from anuncio, usuario where anuncio.status = 'ativo' and  usuario.id = anuncio.id_usuario and anuncio.titulo LIKE '%$retorno_pesquisa%'";
                $busca = mysqli_query($conexao, $sql);
                $total = mysqli_num_rows($busca);

                $num_pagina = ceil($total/$itens_por_pagina);

                $inicio = ($itens_por_pagina*$pagina)-$itens_por_pagina;

                $sql = "SELECT anuncio.id, anuncio.titulo, usuario.nome, usuario.foto, anuncio.tarifa from anuncio, usuario where anuncio.status = 'ativo' and  usuario.id = anuncio.id_usuario and anuncio.titulo LIKE '%$retorno_pesquisa%' limit $inicio, $itens_por_pagina";
                $busca = mysqli_query($conexao, $sql);
                

                if($total == 0){
                    echo "<meta HTTP-EQUIV='refresh' CONTENT='0;URL=erro_busca.php'>";
                }

                unset($_SESSION['pesquisar']);

            } else {
                $sql = "SELECT anuncio.id, anuncio.titulo, usuario.nome, usuario.foto, anuncio.tarifa from anuncio, usuario where anuncio.status = 'ativo' and  usuario.id = anuncio.id_usuario";
                $busca = mysqli_query($conexao, $sql);
                $total = mysqli_num_rows($busca);

                $num_pagina = ceil($total/$itens_por_pagina);

                $inicio = ($itens_por_pagina*$pagina)-$itens_por_pagina;

                $sql = "SELECT anuncio.id, anuncio.titulo, usuario.nome, usuario.foto, anuncio.tarifa from anuncio, usuario where anuncio.status = 'ativo' and  usuario.id = anuncio.id_usuario limit $inicio, $itens_por_pagina";
                $busca = mysqli_query($conexao, $sql);

            }

            while ($array = mysqli_fetch_array($busca)){
                $titulo = $array['titulo'];
                $nome_completo = $array['nome'];
                $nome_usuario = explode(" ", $nome_completo);
                $foto = $array['foto'];
                $tarifa = $array['tarifa'];
                $id_anuncio = $array['id'];


                ?>


                <section class="col-sm-12 col-md-6 col-lg-4 text-center">
                    <div class="card mb-3" style="border-radius: 15px;">
                        <div class="card-body">
                            <h5 class="card-title p-1" id="title"><?php echo $nome_usuario[0] ?></h5>

                            <?php

                            if (!isset($_SESSION['usuarioLogado'])){
                                
                                if($foto == null){ ?>

                            <a href="" data-toggle="modal" data-target="#myModal-login">
                                <img class="card-img-top rounded-circle z-depth-0" src="img/foto.png"
                                    alt="Imagem de capa do card" alt="Imagem de capa do card"
                                    style="width:200px; height:200px; border: 2px solid #170085 !important;"></a>

                            <?php }else { ?>

                            <a href="" data-toggle="modal" data-target="#myModal-login">
                                <img class="card-img-top rounded-circle z-depth-0" src="./imagens/<?php echo $foto ?>"
                                    alt="Imagem de capa do card" alt="Imagem de capa do card"
                                    style="width:200px; height:200px; border: 2px solid #170085 !important;"></a>

                            <?php }}
                                        
                                        if (isset($_SESSION['usuarioLogado'])){
                                            
                                            if($foto == null){ ?>

                            <a href="detalhe_anuncio.php?id=<?= $id_anuncio; ?>&solicitacao=0">
                                <img class="card-img-top rounded-circle z-depth-0" src="img/foto.png"
                                    alt="Imagem de capa do card" alt="Imagem de capa do card"
                                    style="width:200px; height:200px; border: 2px solid #170085 !important;"></a>

                            <?php }else { ?>

                            <a href="detalhe_anuncio.php?id=<?= $id_anuncio; ?>&solicitacao=0">
                                <img class="card-img-top rounded-circle z-depth-0" src="./imagens/<?php echo $foto ?>"
                                    alt="Imagem de capa do card" alt="Imagem de capa do card"
                                    style="width:200px; height:200px; border: 2px solid #170085 !important;"></a>



                            <?php }} ?>


                        </div>

                        <div class="col-12">
                            <h6 class="card-title p-1 ml-1" id="title"><strong><?php echo $titulo?></strong></h6>
                            <h5 class="card-title p-1 ml-1" id="title"><?php echo $tarifa?>/h</h5>
                        </div>


                        <footer class="card-footer" style="background-color:#170085;"></footer>

                    </div>
                </section>


                <?php } ?>


            </div>

            <?php
				//Verificar a pagina anterior e posterior
                                $pagina_anterior = $pagina - 1;
                                $pagina_posterior = $pagina + 1;
                                ?>
            <nav aria-label="Navegação de página">
                <ul class="pagination justify-content-center">
                    <li class="page-item">
                        <?php
                                            if($pagina_anterior != 0){ ?>
                        <a class="page-link" href="index.php?pagina=<?php echo $pagina_anterior; ?>"
                            aria-label="Anterior">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                        <?php }else{ ?>
                    <li class="page-item disabled">

                    </li>

                    <?php }  ?>
                    </li>
                    <?php 
					//Apresentar a paginacao
                                        for($i = 1; $i < $num_pagina + 1; $i++){ 

                                            $estilo ="";
                                            if($pagina == $i){
                                                $estilo = "class=\"page-item active\"";
                                            } ?>

                    <li <?php echo $estilo; ?>><a class="page-link"
                            href="index.php?pagina=<?php echo $i; ?>"><?php echo $i; ?></a></li>
                    <?php } ?>
                    <li class="page-item">
                        <?php
                                                if($pagina_posterior <= $num_pagina){ ?>
                        <a class="page-link" href="index.php?pagina=<?php echo $pagina_posterior; ?>"
                            aria-label="Próximo">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                        <?php }else{ ?>
                    <li class="page-item disabled">

                    </li>
                    <?php }  ?>
                    </li>
                </ul>
            </nav>

        </section>

    </main>

    <?php
                            include 'rodape.php';
                            ?>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>
    <script src="http://propeller.in/components/global/js/global.js"></script>
    <script type="text/javascript" src="http://propeller.in/components/textfield/js/textfield.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
    <script src="https://kit.fontawesome.com/1d6f563437.js" crossorigin="anonymous"></script>

    <script type="text/javascript">
    $("#telefone").mask("(99) 99999-9999");
    $("#data").mask("99/99/9999");
    $("#campoCpf").mask("000.000.000-00");
    </script>

    <script>
    function validaSenha(input) {
        if (input.value != document.getElementById('txtSenha').value) {
            input.setCustomValidity('Repita a senha corretamente');

        } else {

            input.setCustomValidity('');
        }
    }
    </script>



    <?php if($cadastroInvalido == true){ ?>

    <script type="text/javascript">
    $(function() {
        $('#myModal-cadastro').modal('show');
    });
    </script>

    <?php $cadastroInvalido = false; } else if ($loginInvalido == true) { ?>

    <script type="text/javascript">
    $(function() {
        $('#myModal-login').modal('show');
    });
    </script>


    <?php $loginInvalido = false;  } ?>


</body>


</html>