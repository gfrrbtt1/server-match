
<footer id="myFooter">

    <div class="container">
        <hr>     
        <div class="row">
            <div class="col-sm-6 info">
                <h2 class="logo pt-3"><a href="index.php"><img src="img/1.png" width="260px" height="80px"
                    alt=""></a>
                </h2>
                <p>O Server Match te ajuda a encontrar e contratar professores de tecnologia
                em qualquer parte do Brasil.</p> 
            </div>

            <div class="col-sm-1">
            </div>

            <div class="col-sm-2">
                <h5>Sobre nós</h5>
                <ul>
                    <li><a href="quem_somos.php">Quem somos</a></li>
                    <li><a href="como_funciona.php">Como funciona?</a></li>
                </ul>
            </div>
            <div class="col-sm-2">
                <h5>Suporte</h5>
                <ul>
                    <li><a href="#">Central de ajuda</a></li>
                    <li><a href="#">Termos e Política de Privacidade</a></li>
                </ul>
            </div>
            <div class="col-sm-1">
                <ul>
                    <li><a href="#cabecalho"><i class="fas fa-arrow-circle-up"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="second-bar">
        <div class="footer-copyright">
            <p>© 2019 Server Match - Todos os direitos reservados</p>
        </div>
    </div>

</div>
</footer>