<section id="myModal-login" class="modal" tabindex="-1" role="dialog">
    <!-- Início do código da janela modal LOGIN -->
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header m-image">
                <!-- Header do modal -->
                <h4 class="modal-title p-2" id="title">Acesse sua conta</h4><br>

                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button><!-- Título -->
            </div>
            <div class="modal-body">

                <p class="text-aux text-center">Se você já possui cadastro no <strong>Server Match</strong>,
                    utilize os mesmos dados para entrar</p>

                <?php 

                if (isset($_POST['btnEntrar'])){

                    include 'validar_usuario.php';

                    if(isset($_SESSION['msg'])){ ?>

                <p class="text-aux text-center" style="color:#e91e63; font-size:0.85em"><strong>Usuário e/ou senha
                        inválidos</strong></p>

                <?php unset($_SESSION['msg']); } 

                    }?>


                <!-- Corpo do modal -->
                <form action="" method="post">

                    <div class="form-row justify-content-center">

                        <div class="col-10 form-group pmd-textfield pmd-textfield-floating-label">
                            <label for="Default" class="control-label">E-mail</label>
                            <input title="Informe seu e-mail" name="usuario" class="form-control text-center"
                                type="email" autocomplete="off" required>

                        </div>

                    </div>

                    <div class="form-row justify-content-center">

                        <div class="col-10 form-group pmd-textfield pmd-textfield-floating-label">
                            <label for="Default" class="control-label">Senha</label>
                            <input title="Informe sua senha" name="senha" class="form-control text-center"
                                type="password" autocomplete="off" required>

                        </div>

                    </div>

                    <div class="form-row justify-content-center">

                        <button class="col-6 btn" id="login" name="btnEntrar" type="submit">Entrar</button>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <!-- Footer do Modal -->
                <div class="col-12 justify-content-center">
                    <span class="text-aux text-center">Você não possui cadastro?<br><a href="" id="link-cadastrar"
                            data-dismiss="modal" data-toggle="modal" data-target="#myModal-cadastro">
                            Crie sua conta</a></span>
                </div>
            </div>
        </div>
    </div>
</section><!-- ##Fim do Modal -->



<section id="myModal-cadastro" class="modal" tabindex="-1" role="dialog">

    <!-- Início do código da janela modal -->
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header m-image">
                <!-- Header do modal -->
                <h4 class="modal-title p-2" id="title">Crie sua conta</h4><br>

                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button><!-- Título -->
            </div>
            <div class="modal-body">

                <p class="text-aux text-center">Criar uma conta é simples e rápido!<br><strong
                        style="font-size:0.85em">Todos os
                        campos são de preenchimento
                        obrigatório.</strong></p>

                <?php 

                    if (isset($_POST['btnCadastro'])){

                        include '_inserir_usuario.php';

                        if(isset($_SESSION['msg'])){ ?>

                <p class="text-aux text-center" style="color:#e91e63; font-size:0.85em"><strong>Opps! O e-mail informado
                        já possui cadastro.</strong></p>

                <?php unset($_SESSION['msg']); } 

                        }?>


                <!-- Corpo do modal -->
                <form action="" method="post">

                    <div class="form-row justify-content-center">

                        <div class="col-10 form-group pmd-textfield pmd-textfield-floating-label">
                            <label for="Default" class="control-label">Nome</label>
                            <input title="Informe seu nome completo" name="nome" class="form-control text-center"
                                type="text" autocomplete="off" maxlength="35" required>

                        </div>

                    </div>

                    <div class="form-row justify-content-center">

                        <div class="col-10 form-group pmd-textfield pmd-textfield-floating-label">
                            <label for="Default" class="control-label">Telefone</label>
                            <input title="Informe seu telefone" id="telefone" name="telefone"
                                class="form-control text-center" type="text" autocomplete="off" required>

                        </div>

                    </div>


                    <div class="form-row justify-content-center">
                        <div class="col-10 form-group pmd-textfield pmd-textfield-floating-label">
                            <label for="Default" class="control-label">E-mail</label>
                            <input title="Informe seu e-mail" name="email" class="form-control text-center" type="email"
                                autocomplete="off" required>

                        </div>
                    </div>

                    <div class="form-row justify-content-center">
                        <div class="col-10 form-group pmd-textfield pmd-textfield-floating-label">
                            <label for="Default" class="control-label">Senha</label>
                            <input id="txtSenha" title="Sua senha deve possuir entre 6 (seis) e 18 (dezoitos) digitos."
                                name="senha" minlength="6" maxlength="18" class="form-control text-center"
                                type="password" autocomplete="off" required>

                        </div>

                    </div>


                    <div class="form-row justify-content-center">

                        <div class="col-10 form-group pmd-textfield pmd-textfield-floating-label">
                            <label for="Default" class="control-label">Confirmar Senha</label>
                            <input name="repetirsenha"
                                title="Sua senha deve ser igual a senha informada no campo anterior."
                                class="form-control text-center" type="password" autocomplete="off" required
                                oninput="validaSenha(this)">

                        </div>
                    </div>


                    <div class="form-row justify-content-center">

                        <button class="col-6 btn" id="login" name="btnCadastro" type="submit">Cadastrar</button>
                    </div>

                </form>


            </div>
            <div class="modal-footer">
                <!-- Footer do Modal -->
                <div class="col-12 justify-content-center">
                    <span class="text-aux">Ja possui cadastro?<br><a href="" id="link-cadastrar" data-dismiss="modal"
                            data-toggle="modal" data-target="#myModal-login">
                            Acesse sua conta</a></span>
                </div>
            </div>
        </div>
    </div>
</section><!-- ##Fim do Modal -->