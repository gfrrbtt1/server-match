<?php

include 'conexao.php';

session_start();

$usuario = $_SESSION['usuarioLogado'];
$id = $_SESSION['idUsuarioLogado'];
$nome = $_SESSION['nomeUsuarioLogado'];

if(!isset($_SESSION['usuarioLogado'])){
    header('Location:index.php');
}


?>


<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Anunciar Aula</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/estilo.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="http://propeller.in/components/textfield/css/textfield.css">


    <style type="text/css">
        .tooltip>.tooltip-inner {
            background-color: #FFFFFF;
            color: #170085;
            border: 1px solid black;
            padding: 15px;
            font-size: 12px;
        }


        .btn-badge {
            background-color: #FFFFFF;
            color: #170085;
            border: 1px solid #170085 !important;
        }

        .borda-pink {
            border-radius: 20px;
            border: 1px solid #fb3c61 !important;
        }

        .color-pink {
            color: #fb3c61;
        }

        textearea {
            resize: none;
            /* impede que o próprio usuário altere o tamanho do textarea */
            overflow-y: auto;
        }
    </style>

</head>

<body>

    <?php
    include 'cabecalho.php';

    if (isset($_POST['pesquisar'])){

        $pesquisar = $_POST['pesquisar'];
        $_SESSION['pesquisar'] = $pesquisar;
        
        echo "<meta HTTP-EQUIV='refresh' CONTENT='0;URL=index.php'>";
        
    }
    ?>

    <main style="background:#f7f7f7;">

        <section class="container pb-4" style="background:#f7f7f7;">


            <div class="row">
                <div class="col-12 ">
                    <h4 style="color:#555555; margin-top: 10px; padding: 10px; text-align:center; border-radius: 20px;">
                    Novo anúncio</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card" style="border-radius: 20px;">
                        <div class="card-body">
                            <div class="form-row justify-content-center">
                                <h5 class="col-10 card-title p-2" id="title">O que você deseja ensinar?</h5>
                            </div>

                            <form action="_inserir_anuncio.php" method="post" style="margin-top: 20px">

                                <div class="form-row justify-content-center">

                                    <div class="col-10 form-group pmd-textfield pmd-textfield-floating-label">

                                        <label for="Large" class="control-label" style="color: #170085">Informe
                                        um título para seu anúncio</label>
                                        <input type="Default" name="titulo" maxlength="30" class="form-control input-group-md"
                                        type="text" autocomplete="off" required>
                                    </div>



                                    <input type="number" class="form-control" name="id" value="<?php echo $id ?>"
                                    style="display: none">


                                    <div class="col-10 form-group pmd-textfield pmd-textfield-floating-label">
                                        <label for="Default" class="control-label mt-2" style="color: #170085">Em
                                            qual
                                        categoria pertence seu anuncio?</label>
                                        <select name="categoria" class="form-control" required>
                                            <option></option>
                                            <option>Mobile</option>
                                            <option>Programacao</option>
                                            <option>Front-end</option>
                                            <option>Infra & Cloud</option>
                                            <option>Banco de dados</option>
                                            <option>Testes de Software</option>
                                            <option>Ferramentas de desenvolvimento</option>
                                            <option>Design & UX</option>
                                            <option>Marketing Digital</option>
                                            <option>Inovacação & Gestão</option>
                                            <option>Aplicações de escritório</option>
                                            <option>Outras tecnologias</option>

                                        </select>
                                    </div>

                                </div>

                                <div class="form-row justify-content-center" id="myGroup">

                                    <div class="col-10 form-group">

                                        <a data-toggle="collapse" data-target="#collapseExample" aria-expanded="false"
                                        aria-controls="collapseExample">
                                        <span class="badge badge-pill btn-badge"><i class="fas fa-info-circle"
                                            style="color:#fb3c61"></i> Mobile</span>
                                        </a>

                                        <a data-toggle="collapse" data-target="#collapseExample2" aria-expanded="false"
                                        aria-controls="collapseExample">
                                        <span class="badge badge-pill btn-badge"><i class="fas fa-info-circle"
                                            style="color:#fb3c61"></i> Programação</span>
                                        </a>

                                        <a data-toggle="collapse" data-target="#collapseExample3" aria-expanded="false"
                                        aria-controls="collapseExample">
                                        <span class="badge badge-pill btn-badge"><i class="fas fa-info-circle"
                                            style="color:#fb3c61"></i> Front-end</span>
                                        </a>

                                        <a data-toggle="collapse" data-target="#collapseExample4" aria-expanded="false"
                                        aria-controls="collapseExample">
                                        <span class="badge badge-pill btn-badge"><i class="fas fa-info-circle"
                                            style="color:#fb3c61"></i> Infra & Cloud</span>
                                        </a>

                                        <a data-toggle="collapse" data-target="#collapseExample5" aria-expanded="false"
                                        aria-controls="collapseExample">
                                        <span class="badge badge-pill btn-badge"><i class="fas fa-info-circle"
                                            style="color:#fb3c61"></i> Banco de dados</span>
                                        </a>

                                        <a data-toggle="collapse" data-target="#collapseExample6" aria-expanded="false"
                                        aria-controls="collapseExample">
                                        <span class="badge badge-pill btn-badge"><i class="fas fa-info-circle"
                                            style="color:#fb3c61"></i> Testes de Software</span>
                                        </a>

                                        <a data-toggle="collapse" data-target="#collapseExample7" aria-expanded="false"
                                        aria-controls="collapseExample">
                                        <span class="badge badge-pill btn-badge"><i class="fas fa-info-circle"
                                            style="color:#fb3c61"></i> Ferramentas de desenvolvimento</span>
                                        </a>

                                        <a data-toggle="collapse" data-target="#collapseExample8" aria-expanded="false"
                                        aria-controls="collapseExample">
                                        <span class="badge badge-pill btn-badge"><i class="fas fa-info-circle"
                                            style="color:#fb3c61"></i> Design & UX</span>
                                        </a>

                                        <a data-toggle="collapse" data-target="#collapseExample9" aria-expanded="false"
                                        aria-controls="collapseExample">
                                        <span class="badge badge-pill btn-badge"><i class="fas fa-info-circle"
                                            style="color:#fb3c61"></i> Marketing Digital</span>
                                        </a>

                                        <a data-toggle="collapse" data-target="#collapseExample10" aria-expanded="false"
                                        aria-controls="collapseExample">
                                        <span class="badge badge-pill btn-badge"><i class="fas fa-info-circle"
                                            style="color:#fb3c61"></i> Inovacação & Gestão</span>
                                        </a>

                                        <a data-toggle="collapse" data-target="#collapseExample11" aria-expanded="false"
                                        aria-controls="collapseExample">
                                        <span class="badge badge-pill btn-badge"><i class="fas fa-info-circle"
                                            style="color:#fb3c61"></i> Aplicaçoes de escritório</span>
                                        </a>

                                    </div>


                                    <div class="col-10 collapse" id="collapseExample" data-parent="#myGroup">
                                        <div class="card card-body borda-pink">
                                            <h6 class="card-title p-2" id="title">Desenvolvimento Mobile</h6>
                                            <p class="card-text">Android, IOS, Swift, Unity, Multiplataforma
                                            (Cordova, React Native e Xamarim), outros</p>
                                        </div>
                                    </div>

                                    <div class="col-10 collapse" id="collapseExample2" data-parent="#myGroup">
                                        <div class="card card-body borda-pink">
                                            <h6 class="card-title p-2" id="title">Linguagens de Programação</h6>
                                            <p class="card-text">Lógica, Algoritmos, Python, PHP, Java, .NET, Ruby &
                                            Rails, C#, C++, C, Delphi, Django, Laravel, Node Js, outros </p>
                                        </div>
                                    </div>

                                    <div class="col-10 collapse" id="collapseExample3" data-parent="#myGroup">
                                        <div class="card card-body borda-pink">
                                            <h6 class="card-title p-2" id="title">Front-end</h6>
                                            <p class="card-text">HTML, CSS, JavaScript, Jquery, WordPress, Angular,
                                            React, Vue.js, MEAN, Bootstrap, Automaçao e Performance, outros </p>
                                        </div>
                                    </div>

                                    <div class="col-10 collapse" id="collapseExample4" data-parent="#myGroup">
                                        <div class="card card-body borda-pink">
                                            <h6 class="card-title p-2" id="title">Infraestrutura & Cloud</h6>
                                            <p class="card-text">Redes, Servidores, Segurança, Linux, Windows,
                                                macOS,
                                            Devops, Docker, Google Cloud, Certificações, outros </p>
                                        </div>
                                    </div>

                                    <div class="col-10 collapse" id="collapseExample5" data-parent="#myGroup">
                                        <div class="card card-body borda-pink">
                                            <h6 class="card-title p-2" id="title">Banco de dados</h6>
                                            <p class="card-text">Projeto de Banco de dados - Fundamentos, Oracle,
                                                MySql,
                                            PostgreSQL, SQL Server, MongoDB, outros </p>
                                        </div>
                                    </div>

                                    <div class="col-10 collapse" id="collapseExample6" data-parent="#myGroup">
                                        <div class="card card-body borda-pink">
                                            <h6 class="card-title p-2" id="title">Testes & Qualidade de Software
                                            </h6>
                                            <p class="card-text">Testes de Software (Manuais, Automatizados, API),
                                                Cucumber, Selenium, BDD, RSpec e TDD, Appium, Robot, Jasmine,
                                                JMeter,
                                            Postman, Capybara, outros </p>
                                        </div>
                                    </div>

                                    <div class="col-10 collapse" id="collapseExample7" data-parent="#myGroup">
                                        <div class="card card-body borda-pink">
                                            <h6 class="card-title p-2" id="title">Ferramentas de desenvolvimento
                                            </h6>
                                            <p class="card-text">Docker, Jenkins, Kubernetes, Jira, Confluence, Git,
                                                Git
                                            HUB, Git Lab, Bitbucket, Zendesk, outros </p>
                                        </div>
                                    </div>

                                    <div class="col-10 collapse" id="collapseExample8" data-parent="#myGroup">
                                        <div class="card card-body borda-pink">
                                            <h6 class="card-title p-2" id="title">Design & UX</h6>
                                            <p class="card-text">Web Design, UX, Prototipação, Design Grafico, 3D e
                                                animacao, Ferramentas de Design, Design de games, outros
                                            </p>
                                        </div>
                                    </div>

                                    <div class="col-10 collapse" id="collapseExample9" data-parent="#myGroup">
                                        <div class="card card-body borda-pink">
                                            <h6 class="card-title p-2" id="title">Marketing Digital</h6>
                                            <p class="card-text">Marketing Digital, Marketing Analytics, SEO, Social
                                                Media, E-commerce, Branding, Facebook Ads, outros
                                            </p>
                                        </div>
                                    </div>

                                    <div class="col-10 collapse" id="collapseExample10" data-parent="#myGroup">
                                        <div class="card card-body borda-pink">
                                            <h6 class="card-title p-2" id="title">Inovacação & Gestão</h6>
                                            <p class="card-text">Scrum, Kanban, LGPD, Big Data, Data Science, ITIL,
                                                COBIT, PMP e CAPM, UML e Processos, Gestão Agil, outros
                                            </p>
                                        </div>
                                    </div>

                                    <div class="col-10 collapse" id="collapseExample11" data-parent="#myGroup">
                                        <div class="card card-body borda-pink">
                                            <h6 class="card-title p-2" id="title">Aplicações de escritório</h6>
                                            <p class="card-text">Noções básicas de informática, Pacote Office,
                                                G-Suite,
                                                Power BI, Sistemas Operacionais, outros
                                            </p>
                                        </div>
                                    </div>


                                </div>

                                <div class="form-row justify-content-center">

                                    <div class="col-10 form-group pmd-textfield pmd-textfield-floating-label">
                                        <label for="Default" class="control-label mt-2" style="color: #170085">Qual
                                            modalidade
                                            de aula você quer ministrar?
                                        </label>
                                        <select name="modalidade" class="form-control" required>
                                            <option></option>
                                            <option>Aula Individual</option>
                                            <option>Aula Grupo</option>
                                            <option>Aula Individual e Grupo</option>

                                        </select>
                                    </div>


                                    <div class="col-10 form-group pmd-textfield pmd-textfield-floating-label">
                                        <label for="Default" class="control-label mt-2" style="color: #170085">Voce
                                        atende o publico de qual nível de aprendizado?</label>
                                        <select name="nivel" class="form-control" required>
                                            <option></option>
                                            <option>Iniciante</option>
                                            <option>Básico</option>
                                            <option>Intermediário</option>
                                            <option>Avançado</option>
                                            <option>Todos os níveis</option>

                                        </select>
                                    </div>


                                    <h5 class="col-10 card-title p-2" id="title">Conte-nos um pouco mais sobre você
                                    </h5>



                                    <div class="col-10 mt-3 form-group pmd-textfield form-group-md">
                                        <label for="Default" class="control-label"
                                        style="color: #170085">Apresente-se</label>
                                        <textarea required class="form-control" maxlength="900" name="apresentacao"
                                        placeholder="Ex: Eu sou engenheiro(a) / estudante... Eu dou aulas desde... Eu sou graduado(a) em..."></textarea>
                                    </div>




                                    <div class="col-10 form-group pmd-textfield form-group-md">
                                        <label for="Default" class="control-label" style="color: #170085">Qual é sua
                                        metodologia?</label>
                                        <textarea required class="form-control" maxlength="900" name="metodologia"
                                        value=""
                                        placeholder="Ex: Meu método de ensino é… Eu abordo a matéria de uma maneira livre / estruturada..."></textarea>
                                    </div>




                                    <h5 class="col-10 card-title p-2" id="title">Tarifas</h5>

                                    <div class="col-10 form-group pmd-textfield pmd-textfield-floating-label">
                                        <label for="Default" class="control-label" style="color: #170085">Sua tarifa
                                            para
                                        uma hora de aula</label>
                                        <input type="Default" name="tarifa" class="form-control input-group-md"
                                        value="" type="text"
                                        onkeypress="$(this).mask('R$ 990')" autocomplete="off" required>
                                    </div>

                                    <div class="col-10 form-group pmd-textfield form-group-md">
                                        <label for="Default" class="control-label mb-1" style="color: #170085">Detalhes
                                        sobre sua tarifa (opcional)</label>
                                        <textarea class="form-control" maxlength="250" name="tarifa_detalhe"
                                        value=""
                                        placeholder="Indique sua tarifa em função ao nível do aluno, pacote de aulas e afins."></textarea>

                                    </div>

                                </div>



                                <div class="form-row justify-content-center" style="margin-top: 40px">
                                    <button type="submit" class="btn-cadastrar">Cadastrar</button>
                                </div>



                            </form>
                        </div>
                    </div>
                </div>


            </div>

        </div>

    </section>

</main>

<?php
include 'rodape.php';
?>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
</script>

<script src="http://propeller.in/components/global/js/global.js"></script>
<script type="text/javascript" src="http://propeller.in/components/textfield/js/textfield.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
<script src="https://kit.fontawesome.com/1d6f563437.js" crossorigin="anonymous"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('[data-toggle="popover"]').popover();
    });
</script>

<script src='js/autosize.js'></script>
<script>
    autosize(document.querySelectorAll('textarea'));
</script>

<!-- Chama o arquivo Bootstrap JavaScript -->
</body>

</html>