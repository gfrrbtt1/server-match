<?php

include 'conexao.php';

session_start();

if(!isset($_SESSION['usuarioLogado'])){
    header('Location:index.php');
}

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Erro</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/estilo.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="http://propeller.in/components/textfield/css/textfield.css">

</head>

<body>


    <?php 
    
    include 'cabecalho.php'; 

    if (isset($_POST['pesquisar'])){

        $pesquisar = $_POST['pesquisar'];
        $_SESSION['pesquisar'] = $pesquisar;
        
        echo "<meta HTTP-EQUIV='refresh' CONTENT='0;URL=index.php'>";

    }
    
    ?>

    <main style="height:400px">

        <section class="col-12 row text-center" style="padding-top:70px;">

            <div class="col-8">
                <h1 style="margin-top:50px; color:#e91e63;">Infelizmente houve uma instabilidade ao processar a solicitação.</h1>
                <hr class="col-7">
                <a href="index.php" role="button">Voltar para a pagina inicial</a>
            </div>
            <div class="col-4"><img src="img/erro.png" alt="foto perfil" style="width:200px; height:200px;">
            </div>
        </section>

    </main>

    <?php
    include 'rodape.php';
    ?>



    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
</script>

<script src="http://propeller.in/components/global/js/global.js"></script>
<script type="text/javascript" src="http://propeller.in/components/textfield/js/textfield.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
<script src="https://kit.fontawesome.com/1d6f563437.js" crossorigin="anonymous"></script>


</body>

</html>


